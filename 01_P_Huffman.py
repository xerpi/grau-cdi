# -*- coding: utf-8 -*-
"""

"""

import math
import heapq

'''
Funciones útiles
'''

def H1(p):
    s = 0.0
    for pi in p:
        if (pi > 0.0):
            s += pi * math.log2(1/pi)
    return s

def LongitudMedia(C,p):
    return sum([len(ci) * pi for ci, pi in zip(C, p)])

'''
Dada una distribucion de probabilidad, hallar un código de Huffman asociado
'''

def Huffman(p):
	c = ['' for i in p]
	minq = [[p[i], [i]] for i in range(len(p))]
	heapq.heapify(minq)

	for n in range(len(minq) - 1):
		c1 = heapq.heappop(minq)
		c2 = heapq.heappop(minq)

		for i in c1[1]:
			c[i] = '0' + c[i]

		for i in c2[1]:
			c[i] = '1' + c[i]

		heapq.heappush(minq, [c1[0] + c2[0], c1[1] + c2[1]])

	return c


'''
Dada la ddp p=[0.80,0.1,0.05,0.05], hallar un código de Huffman asociado,
la entropía de p y la longitud media de código de Huffman hallado.
'''

p=[0.80,0.1,0.05,0.05]
print('Código Huffman:', Huffman(p))
print('Entropía:', H1(p))
print('Longitud media:', LongitudMedia(Huffman(p), p))


'''
Dada la ddp p=[1/n,..../1/n] con n=2**8, hallar un código de Huffman asociado,
la entropía de p y la longitud media de código de Huffman hallado.
'''

n=2**8
p=[1/n for _ in range(n)]

print('Código Huffman:', Huffman(p))
print('Entropía:', H1(p))
print('Longitud media:', LongitudMedia(Huffman(p), p))

'''
Dado un mensaje hallar la tabla de frecuencia de los caracteres que lo componen
'''
def tablaFrecuencias(mensaje):
	freq = {}
	for c in mensaje:
		freq[c] = freq.get(c, 0) + 1
	return {c : f/len(mensaje) for c, f in freq.items()}

'''
Definir una función que codifique un mensaje utilizando un código de Huffman
obtenido a partir de las frecuencias de los caracteres del mensaje.
'''

def EncodeHuffman(mensaje_a_codificar):
	freq = tablaFrecuencias(mensaje_a_codificar)
	c, p = list(freq.keys()), list(freq.values())
	h = Huffman(p)

	mensaje_codificado = ''
	for l in mensaje_a_codificar:
		mensaje_codificado += h[c.index(l)]

	c2m = dict(zip(h, c))

	return mensaje_codificado, c2m

'''
Definir otra función que decodifique los mensajes codificados con la función
anterior.
'''

def DecodeHuffman(mensaje_codificado, c2m):
	M = ''
	i = 0
	while i < len(mensaje_codificado):
		for d in c2m:
			substr = mensaje_codificado[i: i + len(d)]
			if substr == d:
				M += c2m[d]
				i += len(d)
				break

	return M

'''
Ejemplo
'''
mensaje='La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos.'
mensaje_codificado, m2c=EncodeHuffman(mensaje)
mensaje_recuperado=DecodeHuffman(mensaje_codificado,m2c)
print(mensaje_recuperado)
ratio_compresion=8*len(mensaje)/len(mensaje_codificado)
print(ratio_compresion)

