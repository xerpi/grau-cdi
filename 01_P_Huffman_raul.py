# -*- coding: utf-8 -*-
"""

"""

#from cdi20152016Q1 import *

import heapq
import math
from collections import Counter

# C un array de SOLO codigos, p lista de probs
def LongitudMedia(C,p):
    return sum([len(ci) * pi for ci, pi in zip(C, p)])

# Calcula entropia de p lista de probs
def H1(p):
    s = 0.0
    for pi in p:
        if (pi > 0.0):
            s += pi * math.log2(1/pi)
    return s


# dado un alfabeto de n letras, y un codigo q-ario
# devuelve cuantos hay que combinar la primera vez
def first_how_much_combine(n, q = 2):
    # i 2 <= i <= q
    for i in range(2, q + 1):
        if (i%(q - 1) == n%(q - 1)):
            return i

'''print('Probamos si coincide el return con lo esperado')
print(first_how_much_combine(6, 3), 2)
print(first_how_much_combine(7, 4), 4)
print(first_how_much_combine(7, 5), 3)
print(first_how_much_combine(5, 2), 2)'''


'''
Dada una distribucion de probabilidad, hallar un cÃ³digo de Huffman asociado
'''

# Solo funciona con alfabeto A = {0, 1}
# p ordenada
def Huffman(p):
    codigo = [[i, ''] for i in p]
    heap = [[p[i], [i]] for i in range(len(p))]

    for i in range(len(p) - 1):
        h1 = heapq.heappop(heap)
        h2 = heapq.heappop(heap)

        h12_0 = h1[0] + h2[0]
        h12_1 = h1[1] + h2[1]
        h12 = [h12_0, h12_1]

        for j in h1[1]:
            codigo[j][1] = '0' + codigo[j][1]
        for j in h2[1]:
            codigo[j][1] = '1' + codigo[j][1]
        heapq.heappush(heap, h12)

    return codigo

p = [0.1, 0.2, 0.3, 0.4]
C = Huffman(p)
print('p:', p)
print('Huffman:', C)
print('Entropia:', H1(p))
print('Longitud Media:', LongitudMedia([i[1] for i in C], p))
print('')

p = [0.1]*4 + [0.6]
C = Huffman(p)
print('p:', p)
print('Huffman:', C)
print('Entropia:', H1(p))
print('Longitud Media:', LongitudMedia([i[1] for i in C], p))
print('')

p = [0.05]*20
C = Huffman(p)
print('p:', p)
print('Huffman:', C)
print('Entropia:', H1(p))
print('Longitud Media:', LongitudMedia([i[1] for i in C], p))
print('')

'''
Dada la ddp p=[0.80,0.1,0.05,0.05], hallar un cÃ³digo de Huffman asociado,
la entropÃ­a de p y la longitud media de cÃ³digo de Huffman hallado.
'''

p = [0.80,0.1,0.05,0.05]
C = Huffman(p)
print('p:', p)
print('Huffman:', C)
print('Entropia:', H1(p))
print('Longitud Media:', LongitudMedia([i[1] for i in C], p))
print('')

'''
Dada la ddp p=[1/n,..../1/n] con n=2**8, hallar un cÃ³digo de Huffman asociado,
la entropÃ­a de p y la longitud media de cÃ³digo de Huffman hallado.
'''

n=2**8
p=[1/n for _ in range(n)]
C = Huffman(p)
print('p:', p)
print('Huffman:', C)
print('Entropia:', H1(p))
print('Longitud Media:', LongitudMedia([i[1] for i in C], p))
print('')

'''
Dado un mensaje hallar la tabla de frecuencia de los caracteres que lo componen
'''
# devuelve un Counter del estilo {key: value, ...}
def tablaFrecuencias(mensaje):
    tf = Counter(list(mensaje))
    return tf

'''
PRUEBAS DE EncodeHuffman
tf = tablaFrecuencias('aaaaabc')
keys = tf.keys()
values = tf.values()
print(values, keys)
#ordenamos las keys por el orden de values
#ordenamos values
#asi seguimos teniendo la misma correspondecia key[i]-value[i], pero ordenada
keys = [k for (v,k) in sorted(zip(values, keys))]
values = sorted(values)
print(values, keys)

p = H2(values)
#print(p)
C = Huffman(p)
print(C)
C = [i[1] for i in C]

R = [(a, c) for a, c in zip(keys, C)]
print(R)
m2c = dict(R)
print(m2c)
'''

'''
Definir una funciÃ³n que codifique un mensaje utilizando un cÃ³digo de Huffman
obtenido a partir de las frecuencias de los caracteres del mensaje.

Definir otra funciÃ³n que decodifique los mensajes codificados con la funciÃ³n
anterior.
'''

#Dada una lista de frecuencias n, hallar la lista de prob.
def H2(n):
    p = [ni/sum(n) for ni in n]
    return p


def EncodeHuffman(mensaje_a_codificar):
    tf = tablaFrecuencias(mensaje_a_codificar)
    # tf.values() obtiene las frecuencias
    p = H2(tf.values())
    C = Huffman(p)



    tf = tablaFrecuencias(mensaje_a_codificar)
    keys = tf.keys()
    values = tf.values()

    #ordenamos las keys por el orden de values
    #ordenamos values
    #asi seguimos teniendo la misma correspondecia key[i]-value[i], pero ordenada
    keys = [k for (v,k) in sorted(zip(values, keys))]
    values = sorted(values)

    #obtenemos las prob y el codigo asociado
    #nota que prob[i] se asocia a C[i]
    p = H2(values)
    C = Huffman(p)
    # C es de la forma [[prob, codigo]...]. Con esto nos cargamos prob
    C = [i[1] for i in C]

    #creamos el dicionario
    R = [(a, c) for a, c in zip(keys, C)]
    m2c = dict(R)

    mensaje_codificado = [m2c[i] for i in list(mensaje_a_codificar)]
    return mensaje_codificado, m2c

mensaje = 'hola caracola'
codificado, m2c = EncodeHuffman(mensaje)
print(codificado, m2c)

def DecodeHuffman(mensaje_codificado,m2c):

    return mensaje_decodificado


'''
Ejemplo
'''
'''
mensaje='La heroica ciudad dormÃ­a la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no habÃ­a mÃ¡s ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiÃ©ndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montÃ³n, parÃ¡banse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersÃ¡ndose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y habÃ­a pluma que llegaba a un tercer piso, y arenilla que se incrustaba para dÃ­as, o para aÃ±os, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacÃ­a la digestiÃ³n del cocido y de la olla podrida, y descansaba oyendo entre sueÃ±os el monÃ³tono y familiar zumbido de la campana de coro, que retumbaba allÃ¡ en lo alto de la esbeltatorre en la Santa BasÃ­lica. La torre de la catedral, poema romÃ¡ntico de piedra,delicado himno, de dulces lÃ­neas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gÃ³tico, pero, cabe decir, moderado por uninstinto de prudencia y armonÃ­a que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel Ã­ndice depiedra que seÃ±alaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, mÃ¡s flacas que esbeltas, amaneradas, como seÃ±oritas cursis que aprietandemasiado el corsÃ©; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subÃ­a como fuerte castillo, lanzÃ¡ndosedesde allÃ­ en pirÃ¡mide de Ã¡ngulo gracioso, inimitable en sus medidas y proporciones.Como haz de mÃºsculos y nervios la piedra enroscÃ¡ndose en la piedra trepaba a la altura, haciendo equilibrios de acrÃ³bata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenÃ­a, cual imantada, una bola grande debronce dorado, y encima otra mÃ¡s pequenya, y sobre Ã©sta una cruz de hierro que acababaen pararrayos.'
mensaje_codificado, m2c=EncodeHuffman(mensaje)
mensaje_recuperado=DecodeHuffman(mensaje_codificado,m2c)
print(mensaje_recuperado)
ratio_compresion=8*len(mensaje)/len(mensaje_codificado)
print(ratio_compresion)

'''
