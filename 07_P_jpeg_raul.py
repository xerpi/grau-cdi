# -*- coding: utf-8 -*-
"""

"""

import numpy as np
import scipy.ndimage
import time
import matplotlib.pyplot as plt
from scipy.fftpack import dct
from scipy.fftpack import idct
from scipy import misc
        
"""
Matrices de cuantización, estándares y otras
"""

def RGB_to_YCbCr(pixel):
    new_pixel = np.zeros((3))
    new_pixel[0] = (77/256)*pixel[0] + (150/256)*pixel[1] + 
(29/256)*pixel[2]
    new_pixel[1] = -(44/256)*pixel[0] - (87/256)*pixel[1] + 
(131/256)*pixel[2] + 128
    new_pixel[2] = (131/256)*pixel[0] - (110/256)*pixel[1] - 
(21/256)*pixel[2] + 128
    return new_pixel
    
def YCbCr_to_RGB(pixel):
    new_pixel = np.zeros((3))
    new_pixel[0] = pixel[0] + 1.371*(pixel[2] - 128)
    new_pixel[1] = pixel[0] - 0.698*(pixel[2] - 128) - 
0.336*(pixel[1] - 128)
    new_pixel[2] = pixel[0] + 1.732*(pixel[1] - 128)
    return new_pixel

Q_Luminance=np.array([
    [16 ,11, 10, 16,  24,  40,  51,  61],
    [12, 12, 14, 19,  26,  58,  60,  55],
    [14, 13, 16, 24,  40,  57,  69,  56],
    [14, 17, 22, 29,  51,  87,  80,  62],
    [18, 22, 37, 56,  68, 109, 103,  77],
    [24, 35, 55, 64,  81, 104, 113,  92],
    [49, 64, 78, 87, 103, 121, 120, 101],
    [72, 92, 95, 98, 112, 100, 103,  99]])

Q_Chrominance=np.array([
    [17, 18, 24, 47, 99, 99, 99, 99],
    [18, 21, 26, 66, 99, 99, 99, 99],
    [24, 26, 56, 99, 99, 99, 99, 99],
    [47, 66, 99, 99, 99, 99, 99, 99],
    [99, 99, 99, 99, 99, 99, 99, 99],
    [99, 99, 99, 99, 99, 99, 99, 99],
    [99, 99, 99, 99, 99, 99, 99, 99],
    [99, 99, 99, 99, 99, 99, 99, 99]])

# 1 + (i + j)*q
def Q_matrix(r=1):
    m=np.zeros((8,8))
    for i in range(8):
        for j in range(8):
            m[i,j]=(1+i+j)*r
    return m

"""
Implementar la DCT (Discrete Cosine Transform) 
y su inversa para bloques NxN

dct_bloque(p,N)
idct_bloque(p,N)

p bloque NxN

"""
# debe estar normalizado para recuperar el valor
def dct_bloque(p):
    return dct(dct(p, norm='ortho').T, norm='ortho').T

def idct_bloque(p):
    return idct(idct(p, norm='ortho').T, norm='ortho').T

"""
Reproducir los bloques base de la transformación para los 
casos N=4,8
Ver imágenes adjuntas.
"""

N = 4

fig1 = plt.figure()
for i in range(N):
    for j in range(N):
        p = np.zeros((N, N))
        p[i][j] = 1
        img = dct_bloque(p)
        plot = fig1.add_subplot(N, N, i*N + j + 1)
        plot.axis("off")
        plt.imshow(misc.toimage(img, mode = 'L'), cmap = 
"jet")

plt.axis("off")
plt.show()

N = 8

fig2 = plt.figure()
for i in range(N):
    for j in range(N):
        p = np.zeros((N, N))
        p[i][j] = 1
        img = dct_bloque(p)
        plot = fig2.add_subplot(N, N, i*N + j + 1)
        plot.axis("off")
        plt.imshow(misc.toimage(img, mode = 'L'), cmap = 
"jet")

plt.axis("off")
plt.show()


"""
Implementar la función jpeg_gris(imagen_gray) que: 
1. dibuje el resultado de aplicar la DCT y la cuantización 
(y sus inversas) a la imagen de grises 'imagen_gray' 

2. haga una estimación de la ratio de compresión
según los coeficientes nulos de la transformación: 
(#coeficientes/#coeficientes no nulos).

3. haga una estimación del error
Sigma=np.sqrt(sum(sum((imagen_gray-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_gray)**2)))


En este caso optimizar la DCT 
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""

fig3 = plt.figure()

n_bloque = 8
shift_128_mat = np.full((n_bloque, n_bloque), 128)

def jpeg_gris(imagen_gray):
    
    imagen_gray_reconstr = np.zeros(imagen_gray.shape)
    
    for x in range(0, imagen_gray.shape[0], n_bloque):
        for y in range(0, imagen_gray.shape[1], n_bloque):
            # Compression steps
            block = imagen_gray[x: x + n_bloque, y: y + 
n_bloque]

            block_sub = np.subtract(block, shift_128_mat)
            
            block_dct = dct_bloque(block_sub)
            block_q = np.around(np.divide(block_dct, 
Q_Luminance)) # step where information is lost

            # Reconstruction steps
            block_dq = np.multiply(block_q, Q_Luminance)
            block_idct = idct_bloque(block_dq)
            
            block_add = np.add(block_idct, shift_128_mat)
            
            imagen_gray_reconstr[x: x + n_bloque, y: y + 
n_bloque] = block_add
                
    return imagen_gray_reconstr

mandril_img = 
misc.imread('Standard_test_images/mandril_gray.png', 
mode='L')

mandril_q = jpeg_gris(mandril_img)
#mandril_q = np.array(mandril_q, dtype=np.float64) / 255
plt.imshow(mandril_q, cmap="gray")


"""
Implementar la función jpeg_color(imagen_color) que: 
1. dibuje el resultado de aplicar la DCT y la cuantización 
(y sus inversas) a la imagen RGB 'imagen_color' 

2. haga una estimación de la ratio de compresión
según los coeficientes nulos de la transformación: 
(#coeficientes/#coeficientes no nulos).

3. haga una estimación del error para cada una de las 
componentes RGB
Sigma=np.sqrt(sum(sum((imagen_color-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_color)**2)))


En este caso optimizar la DCT 
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""

fig4 = plt.figure()

n_bloque = 8
shift_128_mat = np.full((n_bloque, n_bloque, 3), 128)

def jpeg_color(imagen_color):
    imagen_color_reconstr = np.zeros(imagen_color.shape)
    
    for x in range(0, imagen_color.shape[0], n_bloque):
        for y in range(0, imagen_color.shape[1], n_bloque):
            # Compression steps
            block = imagen_color[x: x + n_bloque, y: y + 
n_bloque]

            block_sub = np.subtract(block, shift_128_mat)
            
            block_dct = np.zeros((n_bloque, n_bloque, 3))
            block_dct[:, :, 0] = dct_bloque(block_sub[:, :, 
0])
            block_dct[:, :, 1] = dct_bloque(block_sub[:, :, 
1])
            block_dct[:, :, 2] = dct_bloque(block_sub[:, :, 
2])

            block_q = np.zeros((n_bloque, n_bloque, 3))
            block_q[:, :, 0] = np.divide(block_dct[:, :, 0], 
Q_Luminance)   # step where information is lost
            block_q[:, :, 1] = np.divide(block_dct[:, :, 1], 
Q_Chrominance) # step where information is lost
            block_q[:, :, 2] = np.divide(block_dct[:, :, 2], 
Q_Chrominance) # step where information is lost
            block_q = np.around(block_q)
            
            # Reconstruction steps
            block_dq = np.zeros((n_bloque, n_bloque, 3))
            block_dq[:, :, 0] = np.multiply(block_q[:, :, 0], 
Q_Luminance)
            block_dq[:, :, 1] = np.multiply(block_q[:, :, 1], 
Q_Chrominance)
            block_dq[:, :, 2] = np.multiply(block_q[:, :, 2], 
Q_Chrominance)

            block_idct = np.zeros((n_bloque, n_bloque, 3))
            block_idct[:, :, 0] = idct_bloque(block_dq[:, :, 
0])
            block_idct[:, :, 1] = idct_bloque(block_dq[:, :, 
1])
            block_idct[:, :, 2] = idct_bloque(block_dq[:, :, 
2])
            
            block_add = np.add(block_idct, shift_128_mat)
            
            imagen_color_reconstr[x: x + n_bloque, y: y + 
n_bloque] = block_add

    return imagen_color_reconstr
  
# falta convertir laimagen de rgb a ycbcr y viceversa
mandril_img = 
misc.imread('Standard_test_images/mandril_color.png', 
mode='RGB')

for x in range(0, mandril_img.shape[0], 1):
    for y in range(0, mandril_img.shape[1], 1):
        mandril_img[x, y] = RGB_to_YCbCr(mandril_img[x, y])

mandril_q = jpeg_color(mandril_img)

for x in range(0, mandril_q.shape[0], 1):
    for y in range(0, mandril_q.shape[1], 1):
        mandril_q[x, y] = YCbCr_to_RGB(mandril_q[x, y])

mandril_q = np.array(mandril_q, dtype=np.float64) / 255      
plt.imshow(mandril_q)



    

"""
#--------------------------------------------------------------------------
Imagen de GRISES

#--------------------------------------------------------------------------
"""


### .astype es para que lo lea como enteros de 32 bits, si no 
se
### pone lo lee como entero positivo sin signo de 8 bits 
uint8 y por ejemplo al 
### restar 128 puede devolver un valor positivo mayor que 128
'''
mandril_gray=scipy.ndimage.imread('Standard_test_images/mandril_gray.png').astype(np.int32)

start= time.clock()
mandril_jpeg=jpeg_gris(mandril_gray)
end= time.clock()
print("tiempo",(end-start))
'''

"""
#--------------------------------------------------------------------------
Imagen COLOR
#--------------------------------------------------------------------------
"""
## Aplico.astype pero después lo convertiré a 
## uint8 para dibujar y a int64 para calcular el error
'''
mandril_color=scipy.misc.imread('Standard_test_images/mandril_color.png').astype(np.int32)

start= time.clock()
mandril_jpeg=jpeg_color(mandril_color)     
end= time.clock()
print("tiempo",(end-start))
 
'''








