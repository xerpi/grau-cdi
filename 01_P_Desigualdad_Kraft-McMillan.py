# -*- coding: utf-8 -*-
"""

"""

def kraft(L, r=2):
	s = 0.0
	for l in L:
		s += 1.0 / pow(r, l)
	return s

'''
Dada la lista L de longitudes de las palabras de un código
r-ario, decidir si pueden definir un código.

'''

def kraft1(L, r=2):
	return kraft(L, r) <= 1


'''
Dada la lista L de longitudes de las palabras de un código
r-ario, calcular el máximo número de palabras de longitud
máxima, max(L), que se pueden añadir y seguir siendo un código.

'''

def kraft2_bruteforce(L, r=2):
	m = max(L)
	n = 0
	L.append(m)
	while kraft1(L, r):
		L.append(m)
		n += 1
	return n

# kraft(L, r) + (1 / pow(r, max(L))) * k <= 1
# k <= (1 - kraft(L, r)) * pow(r, max(L))

def kraft2(L, r=2):
	m = max(L)
	k = (1 - kraft(L, r)) * pow(r, m)
	return int(k)

'''
Dada la lista L de longitudes de las palabras de un
código r-ario, calcular el máximo número de palabras
de longitud Ln, que se pueden añadir y seguir siendo
un código.
'''

# kraft(L, r) + (1 / pow(r, Ln) * k <= 1
# k <= (1 - kraft(L, r)) * pow(r, Ln)

def kraft3(L, Ln, r=2):
	k = (1 - kraft(L, r)) * pow(r, Ln)
	return int(k)

'''
Dada la lista L de longitudes de las palabras de un
código r-ario, hallar un código prefijo con palabras
con dichas longiutudes
'''

# Returns the string of the representation of the int 'n' with base 'base'
def int2base(n, base=2):
	s = ''
	while n >= base:
		s = str(n % base) + s
		n //= base
	s = str(n % base) + s
	return s

def Code(L, r=2):
	C = []
	code = 0
	prev = 0
	for l in sorted(L):
		if prev != l:
			code *= r ** (l - prev)
		C.append(int2base(code, r).zfill(l))
		code += 1
		prev = l
	return C

#L=[2,2,2,4,5]
#print(Code(L))
#print(kraft1(L))

'''
Ejemplo
'''

L=[1,3,5,5,10,3,5,7,8,9,9,2,2,2]
print(sorted(L),' codigo final:', Code(L))
print(kraft1(L))
