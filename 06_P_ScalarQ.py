# -*- coding: utf-8 -*-

from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

imagen = misc.ascent()

'''
plt.imshow(imagen, cmap = plt.cm.gray)
plt.xticks([])
plt.yticks([])
plt.show()
'''

"""
Mostrar la imagen habiendo cuantizado los valores de los píxeles en
2**k niveles, k=1..8

Para cada cuantización dar la ratio de compresión y Sigma

Sigma=np.sqrt(sum(sum((imagenOriginal-imagenCuantizada)**2)))/(n*m)

"""

def scalar_quant(image, k = 8):
	n, m = image.shape
	B = [int((256 / 2 ** k) * i) for i in range(2 ** k + 1)]
	Y = [int((B[i - 1] + B[i]) / 2) for i in range(1, 2 ** k + 1)]
	img_quant = np.zeros(shape = (n, m), dtype = np.int)

	for i in range(m):
		for j in range(n):
			for idx in range(1, len(B) + 1):
				if image[i][j] > B[idx - 1] and image[i][j] <= B[idx]:
					img_quant[i][j] = Y[idx - 1]
					break

	img_quant = misc.toimage(img_quant, mode = 'L')
	sigma = (np.sqrt(sum(sum((image - img_quant) ** 2))) / (n * m))

	return img_quant, sigma


fig1 = plt.figure()

for i in range(1, 8 + 1):
	print("Generating for k =", i, "...")
	img_quant, sigma = scalar_quant(imagen, i)
	plot = fig1.add_subplot(2, 4, i)
	plot.set_title("k = " + str(i) + ", sigma = " + str(sigma))
	plot.axis("off")
	plt.imshow(img_quant, cmap = plt.cm.gray)

plt.axis("off")
plt.show()


"""
Mostrar la imagen cuantizando los valores de los pixeles de cada bloque
n_bloque x n_bloque en 2^k niveles, siendo n_bloque=8 y k=2

Calcular Sigma y la ratio de compresión (para cada bloque
es necesario guardar 16 bits extra para los valores máximos
y mínimos del bloque, esto supone 16/n_bloque**2 bits más por pixel).
"""

def scalar_quant_block(image, n_bloque = 8, k = 8):
	n, m = image.shape
	img_quant = np.zeros(shape = (n, m), dtype = np.int)

	for i in range(0, m, n_bloque):
		for j in range(0, n, n_bloque):
			block = image[i : i + n_bloque, j : j + n_bloque]
			val_min = block.min()
			val_max = block.max()

			B = [int(val_min + ((val_max - val_min) / 2 ** k) * l) for l in range(2 ** k + 1)]
			Y = [int((B[l - 1] + B[l]) / 2) for l in range(1, 2 ** k + 1)]

			for ii in range(n_bloque):
				for jj in range(n_bloque):
					for idx in range(1, len(B) + 1):
						if block[ii][jj] >= B[idx - 1] and (block[ii][jj] < B[idx] or block[ii][jj] == val_max):
							img_quant[i + ii][j + jj] = Y[idx - 1]
							break

	img_quant = misc.toimage(img_quant, mode = 'L')
	sigma = (np.sqrt(sum(sum((image - img_quant) ** 2))) / (n * m))

	return img_quant, sigma



k = 2
n_bloque = 8

fig2 = plt.figure()
print("Generating for n_block =", n_bloque, ", k =", k, "...")
img_quant, sigma = scalar_quant_block(imagen, n_bloque, k)
plot = fig2.add_subplot(1, 1, 1)
plot.set_title("k = " + str(k) + ", sigma = " + str(sigma))
plot.axis("off")
plt.imshow(img_quant, cmap = plt.cm.gray)

plt.axis("off")
plt.show()

# Size of the blocking-quantized image: [(n * m) / (n_bloque^2)] * (n_bloque * n_bloque * k + 16)
n, m = imagen.shape
original_size = n * m * 8
blockquant_size = (n * m) / (n_bloque * n_bloque) * (n_bloque * n_bloque * k + 16)
print("Ratio de compresion:", original_size / blockquant_size)

