# -*- coding: utf-8 -*-
"""

"""

import math
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
from scipy.cluster.vq import vq, kmeans

lena_img = misc.imread('Standard_test_images/lena_gray_512.png', True)
peppers_img = misc.imread('Standard_test_images/peppers_gray.png', True)

lena_n, lena_m = lena_img.shape
lena_img_data_float = np.array(lena_img, dtype = np.float64) / 255

peppers_n, peppers_m = peppers_img.shape
peppers_img_data_float = np.array(peppers_img, dtype = np.float64) / 255

"""
Usando K-means http://docs.scipy.org/doc/scipy/reference/cluster.vq.html
crear un diccionario cuyas palabras sean bloques 8x8 con 512 entradas
para la imagen de Lena.

Dibujar el resultado de codificar Lena con dicho diccionario.

Calcular el error, la ratio de compresión y el número de bits por píxel
"""

num_clusters = 512
block_size = 8

# List of blocks of NxN of the image represented as a vector in an NxN space
lena_block_list = []
for i in range(0, lena_m, block_size):
	for j in range(0, lena_n, block_size):
		lena_block_list.append(lena_img_data_float[i : i + block_size, j : j + block_size].flatten())

# Create k clusters (regions)
lena_codebook, lena_distortion = kmeans(lena_block_list, num_clusters)

lena_code, lena_dist = vq(lena_block_list, lena_codebook)

# Build the image using the code (contains index into the codebook) and the codebook
lena_img_quant = np.zeros(shape = (lena_n, lena_m), dtype = np.float64)
for i in range(0, lena_m, block_size):
	for j in range(0, lena_n, block_size):
		idx = lena_code[math.floor((j + i * (lena_m / block_size)) / block_size)]
		lena_img_quant[i : i + block_size, j : j + block_size] = lena_codebook[idx].reshape(block_size, block_size)

# Before: size = width * height * sizeof(pixel)
# After: size = num_clusters * (sizeof(pixel) * block_size^2) + (width * height / block_size^2) * log2(num_clusters)
size_before = lena_n * lena_m * 8
size_after = 512 * (8 * (8 * 8)) + (lena_n * lena_m / (8 * 8)) * math.log2(512)
print("Lena: Bits per pixel:", size_after / (lena_n * lena_m))
print("Lena: Compression ratio:", size_before / size_after)
print("Lena: Mean error:", np.mean(lena_dist))

"""
Hacer lo mismo con la imagen Peppers (escala de grises)
http://www.imageprocessingplace.com/downloads_V3/root_downloads/image_databases/standard_test_images.zip
"""

# List of blocks of NxN of the image represented as a vector in an NxN space
peppers_block_list = []
for i in range(0, peppers_m, block_size):
	for j in range(0, peppers_n, block_size):
		peppers_block_list.append(peppers_img_data_float[i : i + block_size, j : j + block_size].flatten())

# Create k clusters (regions)
peppers_codebook, peppers_distortion = kmeans(peppers_block_list, num_clusters)

peppers_code, peppers_dist = vq(peppers_block_list, peppers_codebook)

# Build the image using the code (contains index into the codebook) and the codebook
peppers_img_quant = np.zeros(shape = (peppers_n, peppers_m), dtype = np.float64)
for i in range(0, peppers_m, block_size):
	for j in range(0, peppers_n, block_size):
		idx = peppers_code[math.floor((j + i * (peppers_m / block_size)) / block_size)]
		peppers_img_quant[i : i + block_size, j : j + block_size] = peppers_codebook[idx].reshape(block_size, block_size)

# Before: size = width * height * sizeof(pixel)
# After: size = num_clusters * (sizeof(pixel) * block_size^2) + (width * height / block_size^2) * log2(num_clusters)
size_before = peppers_n * peppers_m * 8
size_after = 512 * (8 * (8 * 8)) + (peppers_n * peppers_m / (8 * 8)) * math.log2(512)
print("Peppers: Bits per pixel:", size_after / (peppers_n * peppers_m))
print("Peppers: Compression ratio:", size_before / size_after)
print("Peppers: Mean error:", np.mean(peppers_dist))


"""
Dibujar el resultado de codificar Peppers con el diccionarios obtenido
con la imagen de Lena.

Calcular el error.
"""

peppers2_code, peppers2_dist = vq(peppers_block_list, lena_codebook)

peppers2_img_quant = np.zeros(shape = (peppers_n, peppers_m), dtype = np.float64)
for i in range(0, peppers_m, block_size):
	for j in range(0, peppers_n, block_size):
		idx = peppers2_code[math.floor((j + i * (peppers_m / block_size)) / block_size)]
		peppers2_img_quant[i : i + block_size, j : j + block_size] = lena_codebook[idx].reshape(block_size, block_size)


print("Peppers (using Lena's dictionary): Mean error:", np.mean(peppers2_dist))

# Draw the images
fig1 = plt.figure()

plot1 = fig1.add_subplot(2, 2, 1)
plot1.set_title("Lena")
plot1.axis("off")
plt.imshow(lena_img_quant, cmap = plt.cm.gray)

plot2 = fig1.add_subplot(2, 2, 2)
plot2.set_title("Peppers")
plot2.axis("off")
plt.imshow(peppers_img_quant, cmap = plt.cm.gray)

plot3 = fig1.add_subplot(2, 2, 3)
plot3.set_title("Peppers using Lena's dictionary")
plot3.axis("off")
plt.imshow(peppers2_img_quant, cmap = plt.cm.gray)

plt.axis("off")
plt.show()
