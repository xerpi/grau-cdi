# -*- coding: utf-8 -*-


########################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy import misc

def walsh_matrix(n):
	if n == 1:
		return np.ones((1, 1))
	else:
		old_m = walsh_matrix(n - 1)
		size = 2 ** (n - 1)
		n2 = int(size / 2)
		m = np.zeros((size, size))
		m[0  : n2,     0  : n2] = old_m
		m[n2 : 2 * n2, 0  : n2] = old_m
		m[0  : n2,     n2 : 2 * n2] = old_m
		m[n2 : 2 * n2, n2 : 2 * n2] = -old_m
		return (1 / np.sqrt(2)) * m

def wh_matrix(n):
	w = walsh_matrix(n)
	changes = []

	for i in range(w.shape[0]):
		chg = 0
		cur = w[i][0]
		for j in range(1, w.shape[1]):
			if w[i][j] != cur:
				chg += 1
				cur = w[i][j]
		changes.append(chg)

	wh = np.zeros(w.shape)

	for i in range(len(changes)):
		wh[changes[i]] = w[i]

	return wh

def iwh_matrix(n):
	return wh_matrix(n).transpose()

"""
Implementar la DWHT Discrete Walsh-Hadamard Transform y su inversa
para bloques NxN

dwht_bloque(p,HWH,N)
idwht_bloque(p,HWH,N)

p bloque NxN
HWH matriz de la transformación
"""

def dwht_bloque(p, n_block = 8):
	wh = wh_matrix(int(np.log2(n_block)) + 1)
	return wh.dot(p)

def idwht_bloque(p, n_block = 8):
	iwh = iwh_matrix(int(np.log2(n_block)) + 1)
	return iwh.dot(p)

'''
p = np.array((4, 4))
p_trans = dwht_bloque(p, len(p))
p_inv_trans = idwht_bloque(p_trans, len(p_trans))

print("p:", p)
print("p_trans:", p_trans)
print("p_inv_trans:", p_inv_trans)
'''

"""
Reproducir los bloques base de la transformación para los casos N=4,8 (Ver imágenes adjuntas)
"""

HWH4 = wh_matrix(4 + 1) # 2^4 = 4 * 4
HWH8 = wh_matrix(6 + 1) # 2^6 = 8 * 8


print("Generating for N = 4...")
N = 4
fig1 = plt.figure()
for i in range(0, N * N):
	x0 = int(i % N) * N
	x1 = (int(i % N) + 1) * N
	y0 = int(i / N) * N
	y1 = (int(i / N) + 1) * N

	img = HWH4[x0 : x1, y0 : y1]
	plot = fig1.add_subplot(N, N, i + 1)
	plot.axis("off")
	plt.imshow(misc.toimage(img, mode = 'L'), cmap = plt.cm.gray)

plt.axis("off")
plt.show()

print("Generating for N = 8...")
N = 8
fig2 = plt.figure()
for i in range(0, N * N):
	x0 = int(i % N) * N
	x1 = (int(i % N) + 1) * N
	y0 = int(i / N) * N
	y1 = (int(i / N) + 1) * N

	img = HWH8[x0 : x1, y0 : y1]
	plot = fig2.add_subplot(N, N, i + 1)
	plot.axis("off")
	plt.imshow(misc.toimage(img, mode = 'L'), cmap = plt.cm.gray)

plt.axis("off")
plt.show()

