# -*- coding: utf-8 -*-
"""

"""
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


#Ejercicio hecho junto con Raúl Peñacoba

'''
Dada una lista p, decidir si es una distribución de probabilidad (ddp)
0<=p[i]<=1, sum(p[i])=1.
'''
def es_ddp(p,tolerancia=10**(-5)):
    s = 0
    for pi in p:
        if (pi < 0 or pi > 1):
            return False
        s += pi
    return ((s >= 1 - tolerancia) and (s <= 1 + tolerancia))

#print(es_ddp([0.5,0.5]))
#print(es_ddp([0.5,1.5]))
#print('')

'''
Dado un código C y una ddp p, hallar la longitud media del código.
'''

def LongitudMedia(C,p):
    return sum([len(ci) * pi for ci, pi in zip(C, p)])

#C1 = ['00', '010', '011']
#C2 = ['010', '011', '00']
#p = [0.5, 0.3, 0.2]
#print(LongitudMedia(C1, p))
#print(LongitudMedia(C2, p))
#pprint('')

'''
Dada una ddp p, hallar su entropí­a.
'''
def H1(p):
    s = 0.0
    for pi in p:
        if (pi > 0.0):
            s += pi * math.log2(1/pi)
    return s

#pprint(H1([15./16., 1.0/16.]))
#pprint(H1([0.5, 0.5]))
#pprint('')

'''
Dada una lista de frecuencias n, hallar su entropí­a.
'''
def H2(n):
    p = [ni/sum(n) for ni in n]
    return H1(p)


'''
Ejemplos
'''
C=['001','101','11','0001','000000001','0001','0000000000']
p=[0.5,0.1,0.1,0.1,0.1,0.1,0]
n=[5,2,1,1,1]

print(H1(p))
print(H2(n))
print(LongitudMedia(C,p))


'''
Dibujar H(p,1-p)
'''
#Hallar entropia por cada par i, 1.0 - i

h = []
axis = []

i = 0.0
while i < 1.0:
    p = [i, 1 - i]
    h.append(H1(p))
    axis.append(i)
    i += 0.01

plt.plot(axis, h)
plt.ylabel('p')
plt.xlabel('H([p, 1-p])')
plt.show()

'''
Hallar aproximadamente el máximo de  H(p,q,1-p-q)
'''

#Busca el máximo manualmente
maximo = -1
i = 0.0
while i < 1.0:
    j = 0.0
    while j < 1.0:
        p = [i, j, 1 - i - j]
        h = H1(p)
        maximo = max(maximo, h)
        j += 0.01
    i += 0.01

print('max:', maximo)


#Dibuja gráfica 3D
X = []
Y = []
Z = []
i = 0.0
while i < 1.0:
    j = 0.0
    while j < 1.0:
        p = [i, j, 1 - i - j]
        Z.append(H1(p))
        X.append(i)
        Y.append(j)
        j += 0.01
    i += 0.01


fig = plt.figure()
ax = Axes3D(fig)
ax.plot(X, Y, Z)
ax.view_init(elev=0, azim=0)
fig.show()
