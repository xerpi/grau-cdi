# -*- coding: utf-8 -*-
"""

"""

import numpy as np
import scipy
import scipy.ndimage
import math
pi = math.pi
import matplotlib.pyplot as plt
from scipy import misc

"""
Matrices de cuantización, estándares y otras
"""

Q_Luminance = np.array([
	[16 ,11, 10, 16,  24,  40,  51,  61],
	[12, 12, 14, 19,  26,  58,  60,  55],
	[14, 13, 16, 24,  40,  57,  69,  56],
	[14, 17, 22, 29,  51,  87,  80,  62],
	[18, 22, 37, 56,  68, 109, 103,  77],
	[24, 35, 55, 64,  81, 104, 113,  92],
	[49, 64, 78, 87, 103, 121, 120, 101],
	[72, 92, 95, 98, 112, 100, 103,  99]])

Q_Chrominance = np.array([
	[17, 18, 24, 47, 99, 99, 99, 99],
	[18, 21, 26, 66, 99, 99, 99, 99],
	[24, 26, 56, 99, 99, 99, 99, 99],
	[47, 66, 99, 99, 99, 99, 99, 99],
	[99, 99, 99, 99, 99, 99, 99, 99],
	[99, 99, 99, 99, 99, 99, 99, 99],
	[99, 99, 99, 99, 99, 99, 99, 99],
	[99, 99, 99, 99, 99, 99, 99, 99]])

def Q_matrix(r=1):
	m = np.zeros((8, 8))
	for i in range(8):
		for j in range(8):
			m[i, j] = (1 + i + j) * r
	return m

"""
Implementar la DCT (Discrete Cosine Transform) y su inversa para bloques NxN

dct_bloque(p,N)
idct_bloque(p,N)

p bloque NxN

"""

def dct_matrix(N = 8):
	C = np.zeros((N, N))
	for i in range(N):
		for j in range(N):
			if i == 0:
				C[i][j] = 1 / np.sqrt(N)
			else:
				C[i][j] = np.sqrt(2 / N) * np.cos(((2 * j + 1) * i * pi) / (2 * N))
	return C

def dct_bloque(p, N = 8):
	C = dct_matrix(N)
	return C.dot(p).dot(C.T)

def idct_bloque(p, N = 8):
	C = dct_matrix(N)
	return C.T.dot(p).dot(C)

"""
Reproducir los bloques base de la transformación para los casos N=4,8
Ver imágenes adjuntas.
"""

N = 4
fig1 = plt.figure()
for i in range(N):
	for j in range(N):
		p = np.zeros((N, N))
		p[i][j] = 1
		plot = fig1.add_subplot(N, N, i * N + j + 1)
		plot.axis("off")
		plt.imshow(misc.toimage(idct_bloque(p, N), mode = 'L'), cmap = "jet")

plt.axis("off")
plt.show()

N = 8
fig2 = plt.figure()
for i in range(N):
	for j in range(N):
		p = np.zeros((N, N))
		p[i][j] = 1
		plot = fig2.add_subplot(N, N, i * N + j + 1)
		plot.axis("off")
		plt.imshow(misc.toimage(idct_bloque(p, N), mode = 'L'), cmap = "jet")

plt.axis("off")
plt.show()

"""
Implementar la función jpeg_gris(imagen_gray) que:
1. dibuje el resultado de aplicar la DCT y la cuantización
   (y sus inversas) a la imagen de grises 'imagen_gray'

2. haga una estimación de la ratio de compresión
   según los coeficientes nulos de la transformación:
   (#coeficientes/#coeficientes no nulos).

3. haga una estimación del error
   Sigma=np.sqrt(sum(sum((imagen_gray-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_gray)**2)))


En este caso optimizar la DCT
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""

mat128 = np.full((8, 8), 128)

def jpeg_gris(imagen_gray):
	out = np.zeros(imagen_gray.shape)
	num_0coef = 0

	for i in range(0, imagen_gray.shape[0], 8):
		for j in range(0, imagen_gray.shape[1], 8):
			I = imagen_gray[i : i + 8, j : j + 8]

			w = np.subtract(I, mat128)

			w = dct_bloque(w, 8)
			w = np.around(np.divide(w, Q_Luminance))

			# Calculate number of 0 coefficients
			for k in range(8):
				for l in range(8):
					if w[k][l] == 0:
						num_0coef += 1

			w = np.multiply(w, Q_Luminance)
			w = idct_bloque(w, 8)

			w = np.add(w, mat128)

			out[i : i + 8, j : j + 8] = w

	return out, num_0coef

mandril_gray = misc.imread('Standard_test_images/mandril_gray.png', mode='L')
mandril_gray_q, num_0coef = jpeg_gris(mandril_gray)

print("compression ratio:", (mandril_gray.shape[0] * mandril_gray.shape[1]) / num_0coef)

sigma = np.sqrt(sum(sum((mandril_gray_q-mandril_gray)**2)))/np.sqrt(sum(sum((mandril_gray_q)**2)))
print("sigma:", sigma)

plt.imshow(mandril_gray_q, cmap = "gray")
plt.show()

"""
Implementar la función jpeg_color(imagen_color) que:
1. dibuje el resultado de aplicar la DCT y la cuantización
(y sus inversas) a la imagen RGB 'imagen_color'

2. haga una estimación de la ratio de compresión
según los coeficientes nulos de la transformación:
(#coeficientes/#coeficientes no nulos).

3. haga una estimación del error para cada una de las componentes RGB
Sigma=np.sqrt(sum(sum((imagen_color-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_color)**2)))


En este caso optimizar la DCT
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""

def RGB_to_YCbCr(rgb):
	Y = (77/256) * rgb[0] + (150/256) * rgb[1] + (29/256) * rgb[2]
	Cb = -(44/256) * rgb[0] - (87/256) * rgb[1] + (131/256) * rgb[2] + 128
	Cr = (131/256) * rgb[0] - (110/256) * rgb[1] - (21/256) * rgb[2] + 128
	return np.array([Y, Cb, Cr])

def YCbCr_to_RGB(ycbcr):
	R = ycbcr[0] + 1.371 * (ycbcr[2] - 128)
	G = ycbcr[0] - 0.698 * (ycbcr[2] - 128) - 0.336 * (ycbcr[1] - 128)
	B = ycbcr[0] + 1.732 * (ycbcr[1] - 128)
	return np.array([R, G, B])

def jpeg_color(imagen_color):
	out = np.zeros(imagen_color.shape, dtype=imagen_color.dtype)
	num_0coef = 0

	for i in range(0, imagen_color.shape[0], 8):
		for j in range(0, imagen_color.shape[1], 8):
			I_rgb = imagen_color[i : i + 8, j : j + 8]
			I_ycbcr = [[RGB_to_YCbCr(I_rgb[k][l]) - 128 for l in range(8)] for k in range(8)]

			blockY = [[I_ycbcr[k][l][0] for l in range(8)] for k in range(8)]
			blockCb = [[I_ycbcr[k][l][1] for l in range(8)] for k in range(8)]
			blockCr = [[I_ycbcr[k][l][2] for l in range(8)] for k in range(8)]

			blockY = dct_bloque(blockY, 8)
			blockCb = dct_bloque(blockCb, 8)
			blockCr = dct_bloque(blockCr, 8)

			blockY = np.around(np.divide(blockY, Q_Luminance))
			blockCb = np.around(np.divide(blockCb, Q_Chrominance))
			blockCr = np.around(np.divide(blockCr, Q_Chrominance))

			# Calculate number of 0 coefficients
			for k in range(8):
				for l in range(8):
					if blockY[k][l] == 0 and blockCb[k][l] == 0 and blockCr[k][l] == 0:
						num_0coef += 1

			blockY_inv = np.multiply(blockY, Q_Luminance)
			blockCb_inv = np.multiply(blockCb, Q_Chrominance)
			blockCr_inv = np.multiply(blockCr, Q_Chrominance)

			blockY_inv = idct_bloque(blockY_inv, 8)
			blockCb_inv = idct_bloque(blockCb_inv, 8)
			blockCr_inv = idct_bloque(blockCr_inv, 8)

			for k in range(8):
				for l in range(8):
					out[i + k][j + l] = YCbCr_to_RGB([
						blockY_inv[k][l] + 128,
						blockCb_inv[k][l] + 128,
						blockCr_inv[k][l] + 128
					])

	return out, num_0coef

mandril_color = misc.imread('Standard_test_images/mandril_color.png', mode='RGB')
mandril_color_q, num_0coef = jpeg_color(mandril_color)

print("compression ratio:", (mandril_color.shape[0] * mandril_color.shape[1]) / num_0coef)

sigma = np.sqrt(sum(sum((mandril_color_q-mandril_color)**2)))/np.sqrt(sum(sum((mandril_color_q)**2)))
print("sigma:", sigma)

plt.imshow(mandril_color_q)
plt.show()

"""
#--------------------------------------------------------------------------
Imagen de GRISES

#--------------------------------------------------------------------------
"""

### .astype es para que lo lea como enteros de 32 bits, si no se
### pone lo lee como entero positivo sin signo de 8 bits uint8 y por ejemplo al
### restar 128 puede devolver un valor positivo mayor que 128
'''
mandril_gray=scipy.ndimage.imread('mandril_gray.png').astype(np.int32)

start= time.clock()
mandril_jpeg=jpeg_gris(mandril_gray)
end= time.clock()
print("tiempo",(end-start))
'''

"""
#--------------------------------------------------------------------------
Imagen COLOR
#--------------------------------------------------------------------------
"""
## Aplico.astype pero después lo convertiré a
## uint8 para dibujar y a int64 para calcular el error
'''
mandril_color=scipy.misc.imread('./mandril_color.png').astype(np.int32)

start= time.clock()
mandril_jpeg=jpeg_color(mandril_color)
end= time.clock()
print("tiempo",(end-start))
'''
