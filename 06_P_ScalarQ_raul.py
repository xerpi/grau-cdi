# -*- coding: utf-8 -*-

from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

imagen = misc.ascent()
#imagen = misc.imread('Standard_test_images/lena_gray_512.png')#Leo la imagen
(n,m)=imagen.shape # filas y columnas de la imagen
plt.imshow(imagen, cmap=plt.cm.gray)
plt.xticks([])
plt.yticks([])
plt.title('Original')
plt.show()

"""
Mostrar la imagen habiendo cuantizado los valores de los píxeles en
2**k niveles, k=1..8

Para cada cuantización dar la ratio de compresión y Sigma

Sigma=np.sqrt(sum(sum((imagenOriginal-imagenCuantizada)**2)))/(n*m)

"""

def assing_recons_value(val):
    for i in range(1, len(midpoints)):
        if (midpoints[i] >= val):
            break
    return recons_values[i - 1]

# Example: 1 bit/pixel, pixel values [0, 255]
#          step = 64
#          midpoints = [0, 128, 256]
#          recons_values = [64, 192]

imagen1 = imagen

k = 1
M = 2**k

interval = { min: imagen1.min(), max: imagen1.max() }

step = (interval[max] - interval[min] + 1)/M
step = int(step)

midpoints = [i for i in range(0, interval[max] + 1 + 1, step)]

recons_values = [int((x + y)/2) for x, y in zip(midpoints[1:], midpoints[:-1])]

# Apply function to all values of input
vfunc = np.vectorize(assing_recons_value)
imagen1 = vfunc(imagen1)

lx, ly = imagen1.shape
X, Y = np.ogrid[0:lx, 0:ly]
plt.imshow(imagen1, cmap=plt.cm.gray)
plt.xticks([])
plt.yticks([])
plt.title('Compresión SIN bloques, k: ' + str(k))
plt.show()

"""
Mostrar la imagen cuantizando los valores de los pixeles de cada bloque
n_bloque x n_bloque en 2^k niveles, siendo n_bloque=8 y k=2

Calcular Sigma y la ratio de compresión (para cada bloque
es necesario guardar 16 bits extra para los valores máximos
y mínimos del bloque, esto supone 16/n_bloque**2 bits más por pixel).
"""

imagen1 = imagen

k = 1
n_bloque = 8
M = 2**k

lx, ly = imagen1.shape

for x in range(0, lx, n_bloque):
    for y in range(0, ly, n_bloque):
        sub_img = imagen1[x: x + n_bloque, y: y + n_bloque]

        interval = { min: sub_img.min(), max: sub_img.max() }

        step = (interval[max] - interval[min] + 1)/M
        step = max(1, int(step))

        midpoints = [i for i in range(0, interval[max] + 1 + 1, step)]

        recons_values = [int((x + y)/2) for x, y in zip(midpoints[1:], midpoints[:-1])]

        # Apply function to all values of input
        vfunc = np.vectorize(assing_recons_value)
        imagen1[x: x + n_bloque, y: y + n_bloque] = vfunc(sub_img)

lx, ly = imagen1.shape
X, Y = np.ogrid[0:lx, 0:ly]
plt.imshow(imagen1, cmap=plt.cm.gray)
plt.xticks([])
plt.yticks([])
plt.title('Compresión CON bloques, k: ' + str(k) + ', n_bloque: ' + str(n_bloque))
plt.show()
