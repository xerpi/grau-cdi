# -*- coding: utf-8 -*-


import math
import random

"""
Dado x en [0,1) dar su representacion en binario, por ejemplo
dec2bin(0.625)='101'
dec2bin(0.0625)='0001'

Dada la representación binaria de un real perteneciente al intervalo [0,1)
dar su representación en decimal, por ejemplo

bin2dec('101')=0.625
bin2dec('0001')=0.0625

nb número máximo de bits

"""

def dec2bin(x, nb=100):
	b = ''
	for i in range(nb):
		x2 = x * 2
		b = b + str(math.floor(x2))
		if (x2 == 1):
			break
		x = x2 - math.floor(x2)

	return b

def bin2dec(xb):
	d = 0.0
	for i in range(len(xb)):
		xbi = ord(xb[i]) - ord('0')
		d += xbi / (1 << i + 1)
	return d

"""
Dada una distribución de probabilidad p(i), i=1..n,
hallar su función distribución:
f(0)=0
f(i)=sum(p(k),k=1..i).
"""

def cdf(p):
	p = sorted(p, reverse=True)

	r = [0]
	for i in range(1, len(p) + 1):
		r.append(r[i - 1] + p[i - 1])

	return r;

"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar el intervalo (l,u) que representa al mensaje.

mensaje='ccda'
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
Arithmetic(mensaje,alfabeto,probabilidades)=0.876 0.8776
"""

def Arithmetic(mensaje,alfabeto,probabilidades):
	ddp = cdf(probabilidades)
	d = dict(zip(alfabeto, [i for i in range(len(probabilidades))]))
	l = 0.0
	u = 1.0
	for m in mensaje:
		ll = (u - l) * ddp[d[m]] + l
		uu = (u - l) * ddp[d[m] + 1] + l

		l = ll
		u = uu

	return l, u

"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar la representación binaria de x=r/2**(t) siendo t el menor
entero tal que 1/2**(t)<l-u, r entero (si es posible par) tal
que l*2**(t)<=r<u*2**(t)

mensaje='ccda'
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
EncodeArithmetic1(mensaje,alfabeto,probabilidades)='111000001'
"""

def EncodeArithmetic1(mensaje,alfabeto,probabilidades):
	l, u = Arithmetic(mensaje, alfabeto, probabilidades)
	t = math.floor(math.log2(1 / (u - l)))

	lt = math.ceil(l * (2 ** t))
	ut = math.floor(u * (2 ** t))

	if ut % 2 == 0:
		x = ut
	else:
		x = lt

	return dec2bin(x / (2 ** t))


"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar el código que representa el mensaje obtenido a partir de la
representación binaria de l y u

mensaje='ccda'
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
EncodeArithmetic2(mensaje,alfabeto,probabilidades)='111000001'

"""

def EncodeArithmetic2(mensaje,alfabeto,probabilidades):
	l, u = Arithmetic(mensaje, alfabeto, probabilidades)

	l = dec2bin(l)
	u = dec2bin(u)

	i = 0;
	while (l[i] == u[i] and i < len(l) and i < len(u)):
		i += 1

	if i == len(l):
		return l
	elif u > l[:i] + '1':
		return l[:i] + '1'
	else:
		next_zero = l.index('0', i + 2)
		return l[:i] + '0' + l[i + 1 : next_zero] + '1'


"""
Dada la representación binaria del número que representa un mensaje, la
longitud del mensaje y el alfabeto con su distribución de probabilidad
dar el mensaje original

code='0'
longitud=4
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
DecodeArithmetic(code,longitud,alfabeto,probabilidades)='aaaa'

code='111000001'
DecodeArithmetic(code,4,alfabeto,probabilidades)='ccda'
DecodeArithmetic(code,5,alfabeto,probabilidades)='ccdab'

"""

def DecodeArithmetic(code,n,alfabeto,probabilidades):
	code = bin2dec(code)
	ddp = cdf(probabilidades)
	m = ''

	for i in range(n):
		# Find the F(i) <= code < F(i + 1)
		j = 1
		while j < len(ddp):
			if code < ddp[j]:
				break
			j += 1

		m += alfabeto[j - 1]

		code = (code - ddp[j - 1]) / (ddp[j] - ddp[j - 1])

	return m

'''
Función que compara la longitud esperada del
mensaje con la obtenida con la codificación aritmética
'''

def comparacion(mensaje,alfabeto,probabilidades):
    p=1.
    indice=dict([(alfabeto[i],i+1) for i in range(len(alfabeto))])
    for i in range(len(mensaje)):
        p=p*probabilidades[indice[mensaje[i]]-1]
    aux=-math.log(p,2), len(EncodeArithmetic1(mensaje,alfabeto,probabilidades)), len(EncodeArithmetic2(mensaje,alfabeto,probabilidades))
    print('Información y longitudes:',aux)
    return aux

'''
Generar 10 mensajes aleatorios M de longitud 10<=n<=20 aleatoria
con las frecuencias esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e', codificarlo y compararlas longitudes
esperadas con las obtenidas.
'''


alfabeto=['a','b','c','d','e']
probabilidades=[0.5,0.2,0.15,0.1,.05]
U = 50*'a'+20*'b'+15*'c'+10*'d'+5*'e'
def rd_choice(X,k = 1):
    Y = []
    for _ in range(k):
        Y +=[random.choice(X)]
    return Y

l_max=20

for _ in range(10):
    n=random.randint(10,l_max)
    L = rd_choice(U, n)
    mensaje = ''
    for x in L:
        mensaje += x
    print('---------- ',mensaje)
    C = comparacion(mensaje,alfabeto,probabilidades)
    print(C)

'''
Generar 10 mensajes aleatorios M de longitud 10<=n<=100 aleatoria
con las frecuencias esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e' y codificarlo.
'''

alfabeto=['a','b','c','d','e']
probabilidades=[0.5,0.2,0.15,0.1,.05]
U = 50*'a'+20*'b'+15*'c'+10*'d'+5*'e'
def rd_choice(X,k = 1):
    Y = []
    for _ in range(k):
        Y +=[random.choice(X)]
    return Y

l_max=100

for _ in range(10):
    n=random.randint(10,l_max)
    L = rd_choice(U, n)
    mensaje = ''
    for x in L:
        mensaje += x
    print('---------- ',mensaje)
    C = EncodeArithmetic1(mensaje,alfabeto,probabilidades)
    print(C)
