# -*- coding: utf-8 -*-

import random
from random import randint

'''
0. Dada una codificación R, construir un diccionario para codificar m2c y otro para decodificar c2m
'''
R = [('a','0'), ('b','11'), ('c','100'), ('d','1010'), ('e','1011')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])


'''
1. Definir una función Encode(M, m2c) que, dado un mensaje M y un diccionario
de codificación m2c, devuelva el mensaje codificado C.
'''

def Encode(M, m2c):
	C = ''
	for m in M:
		C += m2c[m]
	return C

# print(Encode(['a', 'e', 'b'], m2c))

'''
2. Definir una función Decode(C, m2c) que, dado un mensaje codificado C y un diccionario
de decodificación c2m, devuelva el mensaje original M.
'''
def Decode(C, c2m):
	M = ''
	i = 0
	while i < len(C):
		found = False
		for d in c2m:
			substr = C[i: i + len(d)]
			if substr == d:
				M += c2m[d]
				i += len(d)
				found = True
				break
		if not found:
			print("Error decoding", C)
			return '?'
	return M

#print(Decode(Encode(['b', 'e', 'd', 'a'], m2c), c2m))

#------------------------------------------------------------------------
# Ejemplo 1
#------------------------------------------------------------------------

R = [('a','0'), ('b','11'), ('c','100'), ('d','1010'), ('e','1011')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
3. Generar un mensaje aleatorio M de longitud 50 con las frecuencias
esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e' y codificarlo.
'''
freq_table = [('a', 50), ('b', 20), ('c', 15), ('d', 10), ('e', 5)]

def rand_msg(length, freq):
	M = ''
	for i in range(0, length):
		done = False
		while not done:
			idx = randint(0, len(freq) - 1)
			r = randint(1, 100)
			if r <= freq[idx][1]:
				M += freq[idx][0]
				done = True
	return M

M = rand_msg(50, freq_table)
C = Encode(M,m2c)

'''
4. Si 'a', 'b', 'c', 'd', 'e' se codifican inicialmente con un código de
bloque de 3 bits, hallar la ratio de compresión al utilizar el nuevo código.
'''

# Ratio medio:
#    orig_size = (0.5 + 0.2 + 0.15 + 0.10 + 0.05) * 3 * n = 3 * n
#    new_size = ((0.5 * 1) + (0.20 * 2) + (0.15 * 3) + (0.10 * 4) + (0.05 * 4)) * n = 1.95 * n
#    R_medio = 3/1.95 = 1.53846153846

#r = orig_size / new_size =>
r = (len(M) * 3) / len(C)



#------------------------------------------------------------------------
# Ejemplo 2
#------------------------------------------------------------------------
R = [('a','0'), ('b','10'), ('c','110'), ('d','1110'), ('e','1111')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
5.
Codificar y decodificar 20 mensajes aleatorios de longitudes también aleatorios.
Comprobar si los mensajes decodificados coinciden con los originales.
'''

correct = True
for i in range(1, 20):
	length = randint(1, 100)
	M = rand_msg(length, freq_table)
	C = Encode(M, m2c)
	D = Decode(C, c2m)
	if D != M:
		correct = False
		break

if correct:
	print("Good encoding/decoding.")
else:
	print("Bad encoding/decoding!")


#------------------------------------------------------------------------
# Ejemplo 3
#------------------------------------------------------------------------
R = [('a','0'), ('b','01'), ('c','011'), ('d','0111'), ('e','1111')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
6. Codificar y decodificar los mensajes  'ae' y 'be'.
Comprobar si los mensajes decodificados coinciden con los originales.
'''

M1 = 'ae'
C1 = Encode(M1, m2c)
D1 = Decode(C1, c2m)

print(M1, ": Encode() =", C1, "Decode() =", D1)

M2 = 'be'
C2 = Encode(M2, m2c)
D2 = Decode(C2, c2m)

print(M2, ": Encode() =", C2, "Decode() =", D2)

#mi algoritmo de decodificación solo funciona correctamente con códigos prefijo,
#una forma de poder decodificar códigos no prefijo es usando la técnica de backtracking
